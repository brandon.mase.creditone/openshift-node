//* Actual server is express.
const express = require('express');

//* This automatically converts the request response return to JSON instead of having to parse it manually.
const bodyParser = require('body-parser');

//* HTTP Requests
const axios = require('axios');

//* Be able to use the .env file variables.
//* This is so you don't have to upload your API keys to a repo. You would just add the file to Openshift
//* Any variable in the .env file would be used like process.env.<YOUR VARIABLE NAME>
require('dotenv').config();

//* This is what we use to connect to a POSTGRES db. If we are using anything else we just need to find a package that integrates into it.
const massive = require('massive');

//* Connect to the POSTGRES db.
//* Connect with the CONNECTION_STRING variable from the .env file.
//* .env files won't be uploaded to gitlab
//* Create a .env file in the root and add a CONNECTION_STRING = <YOUR CONNECTION STRING> to be able to use this function.
// massive(process.env.CONNECTION_STRING).then(db => app.set('db', db)).catch(e => console.log("massive error", e));
const app = express();


app.use(bodyParser.json());



//* The port number to run the server;
const PORT = 8080;

app.get('/', (res, req) => req.json({
    greeting: 'hello world'
}))
//* This starts the server at localhost:4000
app.listen(PORT, () => console.log(`Listening on PORT ${PORT}`));